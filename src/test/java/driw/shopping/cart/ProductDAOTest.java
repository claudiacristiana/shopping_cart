package driw.shopping.cart;

import driw.shopping.cart.Application;
import driw.shopping.cart.domain.Product;
import driw.shopping.cart.persistence.ProductDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by cristina on 2/18/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductDAOTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductDAO productDAO;

    @Test
    public void testGetProducts(){
        Product product = new Product();
        product.setIdProduct(1);
        product.setProductName("ProductTest");
        entityManager.persist(product);
        entityManager.flush();

       Product result = productDAO.getProduct(1);
        assertEquals(result.getProductName(), product.getProductName());
    }
}