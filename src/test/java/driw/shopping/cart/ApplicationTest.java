package driw.shopping.cart;

import driw.shopping.cart.domain.ShoppingCart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by cristina on 2/18/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {
        @Autowired
        private TestRestTemplate restTemplate;

        @Test
        public void createClient() {
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setAuxIdProduct(1);
            shoppingCart.setAuxPacketNo(5000);
            shoppingCart.setAuxProductNo(1);
            ResponseEntity<Map> responseEntity =
                    restTemplate.postForEntity("/addShoppingCart", shoppingCart, Map.class);
            Map response = responseEntity.getBody();

            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertEquals(true, response.get("success"));
        }


}