package driw.shopping.cart.persistence;

import driw.shopping.cart.domain.Product;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/17/2018.
 */
@Repository
public class ProductDAO {
    @Autowired
    private SessionFactory sessionFactory;

       public List<Map> getProducts() {
        return sessionFactory.getCurrentSession().createSQLQuery("select * from product").setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }


    public Product getProduct(Integer idProduct) {
        return sessionFactory.getCurrentSession().get(Product.class, idProduct);
    }


}
