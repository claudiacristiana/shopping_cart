package driw.shopping.cart.persistence;

import driw.shopping.cart.domain.Product;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/17/2018.
 */
@Repository
public class ShoppingCartDAO {

    @Autowired
    private SessionFactory sessionFactory;


    public void addShoppingCart(driw.shopping.cart.domain.ShoppingCart shoppingCart) {
        shoppingCart.setProductByFkProduct(new Product(shoppingCart.getAuxIdProduct()));
        sessionFactory.getCurrentSession().save(shoppingCart);
    }

    public List<Map> getShoppingCartList() {
        String queryString = "select p.product_name, sc.total_packet_no, sc.total_product_no, sc.total_price, to_char(sc.timestamp_cart, 'yyyy.MM.dd HH:mi:ss') as timestamp_cart  from " +
                "shopping_cart sc inner join product p on sc.fk_product = p.id_product order by sc.timestamp_cart DESC";
        return sessionFactory.getCurrentSession().createSQLQuery(queryString).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
    }
}
