package driw.shopping.cart.persistence;

import driw.shopping.cart.domain.PriceRegulation;
import driw.shopping.cart.util.CartException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by cristina on 2/17/2018.
 */
@Repository
public class PriceRegulationDAO  {

    public static final String DISCOUNT_QUERY = "select p.discount_factor from price_regulation p where p.fk_product=:idProduct and p.min_no_product <=:numberOfProducts and p.max_no_product>=:numberOfProducts";
    @Autowired
    private SessionFactory sessionFactory;

    public Float getDiscountFactor(Integer idProduct, Integer numberOfProducts) throws CartException {
        List<Float> priceRegulationList = sessionFactory.getCurrentSession()
                .createSQLQuery(DISCOUNT_QUERY)
                .setInteger("idProduct", idProduct)
                .setInteger("numberOfProducts", numberOfProducts)
                .list();
        if (priceRegulationList.size() == 1) return priceRegulationList.get(0);
        else {
            throw new CartException("None or more discount factors found! Reconfigure limits!");
        }
    }
}
