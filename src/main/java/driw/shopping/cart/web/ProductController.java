package driw.shopping.cart.web;

import driw.shopping.cart.domain.Product;
import driw.shopping.cart.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/17/2018.
 */
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(path="/getListProducts")
    public Map getListProducts(){
            Map response =new HashMap<>();
            List<Map> result = productService.getProductsList();
            response.put("data",result);
            response.put("total", result.size());
            response.put("success", true);
            return response;
    }
}
