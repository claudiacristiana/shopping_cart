package driw.shopping.cart.web;

import driw.shopping.cart.domain.ShoppingCart;
import driw.shopping.cart.service.EstimationService;
import driw.shopping.cart.service.ProductService;
import driw.shopping.cart.service.ShoppingCartService;
import driw.shopping.cart.util.CartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/18/2018.
 */
@RestController
public class EstimationController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @RequestMapping(path = "/getEstimationProducts")
    public Map getEstimationProducts() {
        Map response =new HashMap<>();
        int counter =0;
        List<ShoppingCart> productList=new ArrayList<>();
        for (Map map : productService.getProductsList()) {
            counter++;
            for(int i=1; i<=50; i++){
                ShoppingCart shoppingCart= new ShoppingCart();
                shoppingCart.setAuxIdProduct((Integer)map.get("id_product"));
                shoppingCart.setAuxPacketNo(0);
                shoppingCart.setAuxProductNo(i);
                try {
                    shoppingCartService.calculatePrice(shoppingCart);
                } catch (CartException e) {
                    response.put("success", false);
                    response.put("message", e.getMessage());
                }
                productList.add(shoppingCart);
            }

        }
        response.put("data",productList);
        response.put("success", true);
        return response;
    }
}
