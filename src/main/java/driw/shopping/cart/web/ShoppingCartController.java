package driw.shopping.cart.web;

import driw.shopping.cart.domain.Product;
import driw.shopping.cart.domain.ShoppingCart;
import driw.shopping.cart.service.ShoppingCartService;
import driw.shopping.cart.util.CartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/17/2018.
 */
@RestController
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;
    @RequestMapping(path="/addShoppingCart", method = RequestMethod.POST)
    public Map addShoppingCart(@RequestBody ShoppingCart shoppingCart) {
        Map response =new HashMap<>();
        try {
            shoppingCartService.addShoppingCart(shoppingCart);
        } catch (CartException e) {
            response.put("success", false);
            response.put("message", e.getMessage());
            return response;
        }
        response.put("success", true);
        response.put("message", "");
        return response;
    }

    @RequestMapping(path="/getListShoppingCart")
    public Map getListShoppingCart(){
        Map response =new HashMap<>();
        List<Map> result = shoppingCartService.getShoppingCartList();
        response.put("data",result);
        response.put("total", result.size());
        response.put("success", true);
        return response;
    }
}
