package driw.shopping.cart.service;

import driw.shopping.cart.domain.Packet;
import driw.shopping.cart.domain.Product;
import driw.shopping.cart.domain.ShoppingCart;
import driw.shopping.cart.persistence.PriceRegulationDAO;
import driw.shopping.cart.persistence.ProductDAO;
import driw.shopping.cart.persistence.ShoppingCartDAO;
import driw.shopping.cart.util.CartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/17/2018.
 */
@Service
@Transactional
public class ShoppingCartService {
    @Autowired
    private ShoppingCartDAO shoppingCartDAO;
    @Autowired
    private ProductDAO productDAO;
    @Autowired
    private PriceRegulationDAO priceRegulationDAO;


    public void addShoppingCart(ShoppingCart shoppingCart) throws CartException {
        calculatePrice(shoppingCart);
        shoppingCartDAO.addShoppingCart(shoppingCart);
        }

    public void calculatePrice(ShoppingCart shoppingCart) throws CartException {
        Integer productId = shoppingCart.getAuxIdProduct();
        Product product = productDAO.getProduct(productId);
        shoppingCart.setProductName(product.getProductName());
        Packet packet = product.getPackets().get(0);
        Integer numberOfProductsPerPacket = packet.getPacketQuantity();// from db
        Integer numberOfClientPackets = shoppingCart.getAuxPacketNo();// from user
        Integer numberOfClientProducts = shoppingCart.getAuxProductNo(); // from user
        if(numberOfClientProducts >= numberOfProductsPerPacket){
           Integer morePackets = numberOfClientProducts / numberOfProductsPerPacket;
            numberOfClientPackets = numberOfClientPackets +morePackets;
            numberOfClientProducts = numberOfClientProducts % numberOfProductsPerPacket;
        }
        shoppingCart.setTotalPacketNo(numberOfClientPackets);
        shoppingCart.setTotalProductNo(numberOfClientProducts);
        Float discountFactorForProducts =Float.NaN;
        Float discountFactorForPackets =Float.NaN;
        Float totalPriceForPackets = 0f;
        Float totalPriceForProducts = 0f;
        if(numberOfClientPackets*numberOfProductsPerPacket!=0 ) {
            discountFactorForPackets = priceRegulationDAO.getDiscountFactor(productId,numberOfClientPackets*numberOfProductsPerPacket);
            totalPriceForPackets = numberOfClientPackets*packet.getPacketPrice()*(1+discountFactorForPackets);
        }
        if(numberOfClientProducts!=0 ) {
            discountFactorForProducts = priceRegulationDAO.getDiscountFactor(productId,numberOfClientProducts);;
            totalPriceForProducts = numberOfClientProducts*(packet.getPacketPrice()/packet.getPacketQuantity())*(1+discountFactorForProducts);
        }

        shoppingCart.setTotalPrice(totalPriceForPackets+totalPriceForProducts);
    }


    public List<Map> getShoppingCartList() {
        return shoppingCartDAO.getShoppingCartList();
    }
}
