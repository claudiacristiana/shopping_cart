package driw.shopping.cart.service;

import driw.shopping.cart.domain.Product;
import driw.shopping.cart.persistence.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by cristina on 2/17/2018.
 */
@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductDAO productDAO;

    public List<Map> getProductsList() {
        return productDAO.getProducts();
    }
}
