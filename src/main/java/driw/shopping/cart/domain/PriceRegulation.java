package driw.shopping.cart.domain;

import javax.persistence.*;

/**
 * Created by cristina on 2/17/2018.
 */
@Entity
@Table(name = "price_regulation", schema = "public", catalog = "shopping_cart")
public class PriceRegulation {
    private int idPriceRegulation;
    private Integer minNoProduct;
    private Integer maxNoProduct;
    private Float discountFactor;
    private String label;
    private Product productByFkProduct;

    @Id
    @Column(name = "id_price_regulation")
    public int getIdPriceRegulation() {
        return idPriceRegulation;
    }

    public void setIdPriceRegulation(int idPriceRegulation) {
        this.idPriceRegulation = idPriceRegulation;
    }

    @Basic
    @Column(name = "min_no_product")
    public Integer getMinNoProduct() {
        return minNoProduct;
    }

    public void setMinNoProduct(Integer minNoProduct) {
        this.minNoProduct = minNoProduct;
    }

    @Basic
    @Column(name = "max_no_product")
    public Integer getMaxNoProduct() {
        return maxNoProduct;
    }

    public void setMaxNoProduct(Integer maxNoProduct) {
        this.maxNoProduct = maxNoProduct;
    }

    @Basic
    @Column(name = "discount_factor")
    public Float getDiscountFactor() {
        return discountFactor;
    }

    public void setDiscountFactor(Float discountFactor) {
        this.discountFactor = discountFactor;
    }

    @Basic
    @Column(name = "label")
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriceRegulation that = (PriceRegulation) o;

        if (idPriceRegulation != that.idPriceRegulation) return false;
        if (minNoProduct != null ? !minNoProduct.equals(that.minNoProduct) : that.minNoProduct != null) return false;
        if (maxNoProduct != null ? !maxNoProduct.equals(that.maxNoProduct) : that.maxNoProduct != null) return false;
        if (discountFactor != null ? !discountFactor.equals(that.discountFactor) : that.discountFactor != null)
            return false;
        if (label != null ? !label.equals(that.label) : that.label != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPriceRegulation;
        result = 31 * result + (minNoProduct != null ? minNoProduct.hashCode() : 0);
        result = 31 * result + (maxNoProduct != null ? maxNoProduct.hashCode() : 0);
        result = 31 * result + (discountFactor != null ? discountFactor.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "fk_product", referencedColumnName = "id_product")
    public Product getProductByFkProduct() {
        return productByFkProduct;
    }

    public void setProductByFkProduct(Product productByFkProduct) {
        this.productByFkProduct = productByFkProduct;
    }
}
