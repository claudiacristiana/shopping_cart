package driw.shopping.cart.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by cristina on 2/17/2018.
 */
@Entity
public class Product {
    private int idProduct;
    private String productName;
    private List<Packet> packets;
    private List<PriceRegulation> priceRegulationsByIdProduct;
    private List<ShoppingCart> shoppingCartsByIdProduct;

    public Product(int idProduct) {
        this.idProduct = idProduct;
    }

    public Product() {
    }

    @Id
    @Column(name = "id_product")
    @SequenceGenerator(name = "seqGenerator", sequenceName = "sq_product", allocationSize = 5)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGenerator")
    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    @Basic
    @Column(name = "product_name")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (idProduct != product.idProduct) return false;
        if (productName != null ? !productName.equals(product.productName) : product.productName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idProduct;
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "product")
    public List<Packet> getPackets() {
        return packets;
    }

    public void setPackets(List<Packet> packetsByIdProduct) {
        this.packets = packetsByIdProduct;
    }

    @OneToMany(mappedBy = "productByFkProduct")
    public List<PriceRegulation> getPriceRegulationsByIdProduct() {
        return priceRegulationsByIdProduct;
    }

    public void setPriceRegulationsByIdProduct(List<PriceRegulation> priceRegulationsByIdProduct) {
        this.priceRegulationsByIdProduct = priceRegulationsByIdProduct;
    }

    @OneToMany(mappedBy = "productByFkProduct")
    public List<ShoppingCart> getShoppingCartsByIdProduct() {
        return shoppingCartsByIdProduct;
    }

    public void setShoppingCartsByIdProduct(List<ShoppingCart> shoppingCartsByIdProduct) {
        this.shoppingCartsByIdProduct = shoppingCartsByIdProduct;
    }
}
