package driw.shopping.cart.domain;

import javax.persistence.*;

/**
 * Created by cristina on 2/17/2018.
 */
@Entity
public class Packet {
    private int idPacket;
    private Integer packetQuantity;
    private Float packetPrice;
    private Product product;

    @Id
    @Column(name = "id_packet")
    public int getIdPacket() {
        return idPacket;
    }

    public void setIdPacket(int idPacket) {
        this.idPacket = idPacket;
    }

    @Basic
    @Column(name = "packet_quantity")
    public Integer getPacketQuantity() {
        return packetQuantity;
    }

    public void setPacketQuantity(Integer packetQuantity) {
        this.packetQuantity = packetQuantity;
    }

    @Basic
    @Column(name = "packet_price")
    public Float getPacketPrice() {
        return packetPrice;
    }

    public void setPacketPrice(Float packetPrice) {
        this.packetPrice = packetPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Packet packet = (Packet) o;

        if (idPacket != packet.idPacket) return false;
        if (packetQuantity != null ? !packetQuantity.equals(packet.packetQuantity) : packet.packetQuantity != null)
            return false;
        if (packetPrice != null ? !packetPrice.equals(packet.packetPrice) : packet.packetPrice != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPacket;
        result = 31 * result + (packetQuantity != null ? packetQuantity.hashCode() : 0);
        result = 31 * result + (packetPrice != null ? packetPrice.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "fk_product", referencedColumnName = "id_product", nullable = false)
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product productByFkProduct) {
        this.product = productByFkProduct;
    }
}
