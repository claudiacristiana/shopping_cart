package driw.shopping.cart.domain;

import javax.persistence.*;

/**
 * Created by cristina on 2/17/2018.
 */
@Entity
@Table(name = "shopping_cart", schema = "public", catalog = "shopping_cart")
public class ShoppingCart {
    private int idShoppingCart;
    private Integer totalProductNo; //Number of products that are cannot form a packet
    private Integer totalPacketNo; //Total packet number computed based on the total products
    private Float totalPrice;
    private String user;
    private Product productByFkProduct;

    private Integer auxIdProduct;
    private Integer auxProductNo;
    private Integer auxPacketNo;
    private String productName;

    @Transient
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Transient
    public Integer getAuxProductNo() {
        return auxProductNo;
    }

    public void setAuxProductNo(Integer auxProductNo) {
        this.auxProductNo = auxProductNo;
    }
    @Transient
    public Integer getAuxPacketNo() {
        return auxPacketNo;
    }

    public void setAuxPacketNo(Integer auxPacketNo) {
        this.auxPacketNo = auxPacketNo;
    }

    @Transient
    public Integer getAuxIdProduct() {
        return auxIdProduct;
    }

    public void setAuxIdProduct(Integer auxIdProduct) {
        this.auxIdProduct = auxIdProduct;
    }

    @Id
    @Column(name = "id_shopping_cart")
    @SequenceGenerator(name = "seqGenerator", sequenceName = "sq_shopping_cart", allocationSize = 5)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGenerator")
    public int getIdShoppingCart() {
        return idShoppingCart;
    }

    public void setIdShoppingCart(int idShoppingCart) {
        this.idShoppingCart = idShoppingCart;
    }

    @Basic
    @Column(name = "total_product_no")
    public Integer getTotalProductNo() {
        return totalProductNo;
    }

    public void setTotalProductNo(Integer productNo) {
        this.totalProductNo = productNo;
    }

    @Basic
    @Column(name = "total_packet_no")
    public Integer getTotalPacketNo() {
        return totalPacketNo;
    }

    public void setTotalPacketNo(Integer totalPacketNo) {
        this.totalPacketNo = totalPacketNo;
    }

    @Basic
    @Column(name = "total_price")
    public Float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Basic
    @Column(name = "user_name")
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingCart that = (ShoppingCart) o;

        if (idShoppingCart != that.idShoppingCart) return false;
        if (totalProductNo != null ? !totalProductNo.equals(that.totalProductNo) : that.totalProductNo != null) return false;
        if (totalPrice != null ? !totalPrice.equals(that.totalPrice) : that.totalPrice != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idShoppingCart;
        result = 31 * result + (totalProductNo != null ? totalProductNo.hashCode() : 0);
        result = 31 * result + (totalPrice != null ? totalPrice.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "fk_product", referencedColumnName = "id_product")
    public Product getProductByFkProduct() {
        return productByFkProduct;
    }

    public void setProductByFkProduct(Product productByFkProduct) {
        this.productByFkProduct = productByFkProduct;
    }
}
