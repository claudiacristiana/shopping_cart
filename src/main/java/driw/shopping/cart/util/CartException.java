package driw.shopping.cart.util;

/**
 * Created by cristina on 2/17/2018.
 */
public class CartException extends Exception {
    public CartException(String message) {
        super(message);
    }
}
