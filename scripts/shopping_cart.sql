--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.4.5
-- Started on 2018-02-18 17:27:05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 178 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2029 (class 0 OID 0)
-- Dependencies: 178
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 16426)
-- Name: packet; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE packet (
    id_packet integer NOT NULL,
    fk_product integer NOT NULL,
    packet_quantity integer NOT NULL,
    packet_price real NOT NULL
);


ALTER TABLE packet OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16436)
-- Name: price_regulation; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE price_regulation (
    id_price_regulation integer NOT NULL,
    fk_product integer NOT NULL,
    min_no_product integer NOT NULL,
    max_no_product integer NOT NULL,
    discount_factor real NOT NULL,
    label character varying(100)
);


ALTER TABLE price_regulation OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16421)
-- Name: product; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE product (
    id_product integer NOT NULL,
    product_name character varying(100) NOT NULL
);


ALTER TABLE product OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16446)
-- Name: shopping_cart; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE shopping_cart (
    id_shopping_cart integer NOT NULL,
    fk_product integer NOT NULL,
    total_product_no integer,
    total_price real,
    user_name character varying(100),
    total_packet_no integer,
    timestamp_cart timestamp without time zone DEFAULT now()
);


ALTER TABLE shopping_cart OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16458)
-- Name: sq_product; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_product
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_product OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 16456)
-- Name: sq_shopping_cart; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sq_shopping_cart
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sq_shopping_cart OWNER TO postgres;

--
-- TOC entry 2017 (class 0 OID 16426)
-- Dependencies: 173
-- Data for Name: packet; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO packet VALUES (1, 1, 20, 175);
INSERT INTO packet VALUES (2, 2, 5, 825);


--
-- TOC entry 2018 (class 0 OID 16436)
-- Dependencies: 174
-- Data for Name: price_regulation; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO price_regulation VALUES (1, 2, 1, 4, 0.300000012, 'Incresead by 30%');
INSERT INTO price_regulation VALUES (2, 2, 5, 14, 0, 'No discount');
INSERT INTO price_regulation VALUES (3, 2, 15, 1000000, -0.100000001, 'Discount 10%');
INSERT INTO price_regulation VALUES (4, 1, 1, 19, 0.300000012, 'Increase 30%');
INSERT INTO price_regulation VALUES (5, 1, 20, 59, 0, 'No discount');
INSERT INTO price_regulation VALUES (6, 1, 60, 100000, -0.100000001, 'Discount 10%');


--
-- TOC entry 2016 (class 0 OID 16421)
-- Dependencies: 172
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO product VALUES (1, 'Penguins');
INSERT INTO product VALUES (2, 'HorseShoe');


--
-- TOC entry 2019 (class 0 OID 16446)
-- Dependencies: 175
-- Data for Name: shopping_cart; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO shopping_cart VALUES (55, 1, 1, 186.375, NULL, 1, NULL);
INSERT INTO shopping_cart VALUES (56, 2, 2, 2079, NULL, 2, '2018-02-18 00:00:00');
INSERT INTO shopping_cart VALUES (57, 1, 5, 844.375, NULL, 5, '2018-02-18 00:00:00');
INSERT INTO shopping_cart VALUES (58, 1, 6, 1013.25, NULL, 6, '2018-02-18 17:09:45.027');


--
-- TOC entry 2030 (class 0 OID 0)
-- Dependencies: 177
-- Name: sq_product; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_product', 1, false);


--
-- TOC entry 2031 (class 0 OID 0)
-- Dependencies: 176
-- Name: sq_shopping_cart; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sq_shopping_cart', 11, true);


--
-- TOC entry 1899 (class 2606 OID 16430)
-- Name: pk_packet; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY packet
    ADD CONSTRAINT pk_packet PRIMARY KEY (id_packet);


--
-- TOC entry 1901 (class 2606 OID 16440)
-- Name: pk_price_regulation; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY price_regulation
    ADD CONSTRAINT pk_price_regulation PRIMARY KEY (id_price_regulation);


--
-- TOC entry 1897 (class 2606 OID 16425)
-- Name: pk_product; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY product
    ADD CONSTRAINT pk_product PRIMARY KEY (id_product);


--
-- TOC entry 1903 (class 2606 OID 16450)
-- Name: pk_shopping_cart; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY shopping_cart
    ADD CONSTRAINT pk_shopping_cart PRIMARY KEY (id_shopping_cart);


--
-- TOC entry 1904 (class 2606 OID 16431)
-- Name: packet_to_product; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY packet
    ADD CONSTRAINT packet_to_product FOREIGN KEY (fk_product) REFERENCES product(id_product);


--
-- TOC entry 1905 (class 2606 OID 16441)
-- Name: price_regulation_to_products; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY price_regulation
    ADD CONSTRAINT price_regulation_to_products FOREIGN KEY (fk_product) REFERENCES product(id_product);


--
-- TOC entry 1906 (class 2606 OID 16451)
-- Name: shopping_cart_to_product; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY shopping_cart
    ADD CONSTRAINT shopping_cart_to_product FOREIGN KEY (fk_product) REFERENCES product(id_product);


--
-- TOC entry 2028 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-02-18 17:27:05

--
-- PostgreSQL database dump complete
--

